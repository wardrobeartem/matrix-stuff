#include <stdio.h>
#include <math.h>

#define ROWS 4

typedef float matrix [ROWS][ROWS];

matrix A = {{1,2,3,4},
            {0,5,6,7},
            {0,0,2,0},
            {0,0,0,1}};

float B[2][2]= {{3,5},{0,4}};

void findSubMatrix(float *original, float *sub, int indexRow, int indexColumn, int side) {
    float (*o)[side] = (void*) original;
    float (*s)[side-1] = (void*) sub;
    for(int i = 0; i < side-1; i++) {
        for(int j = 0; j < side - 1; j++) {
            s[i][j] = o[i+((i>=indexRow)?1:0)][j+((j>=indexColumn)?1:0)];
        }
    }
}

void printMatrix(float *b, int side) {
    float (*p)[side] = (void*) b;
    for(int i = 0; i < side; i++) {
        for(int j = 0; j < side; j++) {
            printf("%5g ",p[i][j]);
        }
        printf("\n");
    }
}

void transpose(float *b, int side) {
    float (*p)[side] = (void*) b;
    for(int i = 0; i < side; i++)
        for(int j = 0; j < i; j++){
            float tmp = p[j][i];
            p[j][i] = p[i][j];
            p[i][j] = tmp;
        }
}

float det(float *b, int side) {
    float (*p)[side] = (void*) b;
    if(side == 1)
        return p[0][0];
    else if(side == 2)
        return p[0][0] * p[1][1] - p[0][1] * p[1][0];
    else  {
        float sum = 0;
        for(int i = 0; i < side; i++) {

            float subMatrix[side-1][side-1];

            findSubMatrix((float *) p, (float *) subMatrix, i, 0, side);
            sum += p[i][0] * det((float *) subMatrix,side-1) * ((i%2)==0?1:-1);
        }
        return sum;
    }
    return 0;
}

void inverse(float *b, int side){
    float (*p)[side] = (void*) b;
    float recDet = 1 / det(b,side);
    //printf("1/det: %g\n",recDet);
    float subMatrix[side-1][side-1];
    float inverse[side][side];
    for(int i = 0; i < side; i++) {
        for(int j = 0; j < side; ++j) {
            findSubMatrix((float *) p, (float *) subMatrix, i, j, side);
            inverse[i][j] = det((float *) subMatrix, side - 1) * ((((i+j)%2)==0)?1:-1) * recDet;
        }
    }
    for(int i = 0; i < side; i++) {
        for(int j = 0; j < side; ++j) {
            p[i][j] = inverse[i][j];
        }
    }
    transpose((float *) p, side);
}

int main() {
    inverse((float*) A, ROWS);
    printMatrix((float*) A, ROWS);
}
